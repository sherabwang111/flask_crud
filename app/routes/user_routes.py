from flask import jsonify, make_response, Blueprint, request
from app.models.user import User
from app import db
from app.serializers.user_serializer import UserSerializer
import traceback

from werkzeug.utils import secure_filename
import pandas as pd

user_routes = Blueprint('user_routes', __name__)

# Create a user
@user_routes.route('/users', methods=['POST'])
def create_user():
    try:
        data = request.get_json()
        if 'username' not in data or 'email' not in data:
            return make_response(jsonify({'message': 'username and email are required'}), 400)
        new_user = User(username=data['username'], email=data['email'])
        db.session.add(new_user)
        db.session.commit()
        serialized_user = UserSerializer.serialize_user(new_user)
        return make_response(jsonify({'message': 'user created', 'user': serialized_user}), 201)
    except Exception as e:
        return make_response(jsonify({'message': 'error creating user'}), 500)


# Get all users
@user_routes.route('/users', methods=['GET'])
def get_users():
    try:
        users = User.query.all()
        serialized_users = [UserSerializer.serialize_user(user) for user in users]
        return make_response(jsonify(serialized_users), 200)
    except Exception as e:
        traceback.print_exc()
        return make_response(jsonify({'message': 'error getting users'}), 500)

# Get a user by id
@user_routes.route('/users/<int:id>', methods=['GET'])
def get_user(id):
    try:
        user = User.query.get(id)
        if user:
            serialized_user = UserSerializer.serialize_user(user)
            return make_response(jsonify({'user': serialized_user}), 200)
        return make_response(jsonify({'message': 'user not found'}), 404)
    except Exception as e:
        traceback.print_exc()
        return make_response(jsonify({'message': 'error getting user'}), 500)

# Update a user
@user_routes.route('/users/<int:id>', methods=['PUT'])
def update_user(id):
    try:
        user = User.query.get(id)
        if user:
            data = request.get_json()
            user.username = data['username']
            user.email = data['email']
            db.session.commit()

            serialized_user = UserSerializer.serialize_user(user)
            return make_response(jsonify({'message': 'user updated', 'user': serialized_user}), 200)
        return make_response(jsonify({'message': 'user not found'}), 404)
    except Exception as e:
        traceback.print_exc()
        return make_response(jsonify({'message': 'error updating user'}), 500)

# Delete a user
@user_routes.route('/users/<int:id>', methods=['DELETE'])
def delete_user(id):
    try:
        user = User.query.get(id)
        if user:
            db.session.delete(user)
            db.session.commit()
            return make_response(jsonify({'message': 'user deleted'}), 200)
        return make_response(jsonify({'message': 'user not found'}), 404)
    except Exception as e:
        traceback.print_exc()
        return make_response(jsonify({'message': 'error deleting user'}), 500)
    
#uploading csv file
@user_routes.route('/users/csv', methods=['POST'])
def upload_csv():
    try:
        file = request.files['file']
        if file and file.filename.endswith('.csv'):
            # Generate a secure filename and save the file
            filename = secure_filename(file.filename)
            file.save(filename)

            # Read the CSV file using pandas
            df = pd.read_csv(filename)

            # Iterate over the rows and create User objects
            for _, row in df.iterrows():
                new_user = User(username=row['username'], email=row['email'])
                db.session.add(new_user)

            db.session.commit()

            return make_response(jsonify({'message': 'CSV file uploaded and data stored'}), 200)
        else:
            return make_response(jsonify({'message': 'Invalid file format. Please upload a CSV file'}), 400)
    except Exception as e:
        return make_response(jsonify({'message': 'Error uploading CSV file'}), 500)


#excel file upload
@user_routes.route('/users/excel', methods=['POST'])
def upload_excel():
        file = request.files['file']
        if file and file.filename.endswith(('.xls', '.xlsx')):
            # Generate a secure filename and save the file
            filename = secure_filename(file.filename)
            file.save(filename)

            # Read the Excel file using pandas with openpyxl engine
            df = pd.read_excel(filename, engine='openpyxl')

            # Iterate over the rows and create User objects
            for _, row in df.iterrows():
                new_user = User(username=row['username'], email=row['email'])
                db.session.add(new_user)

            db.session.commit()

            return make_response(jsonify({'message': 'Excel file uploaded and data stored'}), 200)
        else:
            return make_response(jsonify({'message': 'Invalid file format. Please upload an Excel file'}), 400)
    

        

