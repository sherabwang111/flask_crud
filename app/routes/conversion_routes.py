from flask import jsonify, make_response, Blueprint, request
import xmltodict

conversion_routes = Blueprint('conversion_routes', __name__)

# XML to JSON Conversion
@conversion_routes.route('/xml-to-json', methods=['POST'])
def xml_to_json():
    try:
        xml_data = request.data
        json_data = xmltodict.parse(xml_data)
        return jsonify(json_data), 200
    except Exception as e:
        return make_response(jsonify({'message': 'error converting XML to JSON'}), 500)

# JSON to XML Conversion
@conversion_routes.route('/json-to-xml', methods=['POST'])
def json_to_xml():
    try:
        json_data = request.get_json()
        xml_data = xmltodict.unparse(json_data, pretty=True)
        return xml_data, 200, {'Content-Type': 'application/xml'}
    except Exception as e:
        return make_response(jsonify({'message': 'error converting JSON to XML'}), 500)
