from flask import Flask
from flask_sqlalchemy import SQLAlchemy

db = SQLAlchemy()

def create_app():
    app = Flask(__name__)
    app.config.from_object('app.config.Config')

    # Initialize the SQLAlchemy extension
    db.init_app(app)

    with app.app_context():
        db.create_all()

    from app.routes.user_routes import user_routes
    from app.routes.conversion_routes import conversion_routes 
    app.register_blueprint(user_routes)
    app.register_blueprint(conversion_routes)

    return app
