import unittest
from app import create_app


class UserRoutesTestCase(unittest.TestCase):

    def setUp(self):
        self.app = create_app().test_client()
        self.app.testing = True

    def test_create_user(self):
        data = {
            'username': 'wangchuk',
            'email': 'wangchuk@dk.bt'
        }
        response = self.app.post('/users', json=data)
        self.assertEqual(response.status_code, 201)

    def test_get_users(self):
        response = self.app.get('/users')
        self.assertEqual(response.status_code, 200)

    def test_get_user(self):
        user_id = 28  
        response = self.app.get(f'/users/{user_id}')
        self.assertIn('user', response.json)

    def test_update_user(self):
        user_id = 29  
        data = {
            'username': 'new_username',
            'email': 'new_email@example.com'
        }
        response = self.app.put(f'/users/{user_id}', json=data)
        self.assertEqual(response.status_code, 200)

    def test_delete_user(self):
        user_id = 30
        response = self.app.delete(f'/users/{user_id}')
        self.assertEqual(response.status_code, 200)


if __name__ == '__main__':
    unittest.main()
